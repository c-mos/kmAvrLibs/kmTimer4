/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//* @file kmTimer4.c
*
*  Created on: 3/15/2024 11:21:16 PM
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Test Application for kmAvrLibs
*  Copyright (C) 2024  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../kmCommon/kmCommon.h"
#ifdef TIMSK4

#include "kmTimer4.h"

#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

#include "../kmTimersCommon/kmTimer4Defs.h"


// Definitions
#define KM_TCC4_TOP KM_TCC_TOP_3

#define KM_TCC4_CYCLES_CALCULATION_CORRECTION 1u

// "Private" variables
static uint8_t _timer4PrescalerSelectBits = 0;

static uint16_t _timer4CompOvfCycles = KM_TCC4_TOP;

static uint16_t _timer4CompACycles = KM_TCC4_TOP;
static bool _timer4OtputComparePinUsedA = false;

#ifdef COM4B0
static uint16_t _timer4CompBCycles = KM_TCC4_TOP;
static bool _timer4OtputComparePinUsedB = false;
#endif /* OCR4B */

#ifdef COM4C0
static uint16_t _timer4CompCCycles = KM_TCC4_TOP;
static bool _timer4OtputComparePinUsedC = false;
#endif /* COM4C0 */

static kmTimer4CallbackType *_timer4CallbackOVF = NULL;
static void *_timer4CallbackUserDataOVF = NULL;

static kmTimer4CallbackType *_timer4CallbackCompA = NULL;
static void *_Timer4CallbackUserDataCompA = NULL;

#ifdef OCR4B
static kmTimer4CallbackType *_timer4CallbackCompB = NULL;
static void *_timer4CallbackUserDataCompB = NULL;
#endif /* OCR4B */

#ifdef COM4C0
static kmTimer4CallbackType *_timer4CallbackCompC = NULL;
static void *_timer4CallbackUserDataCompC = NULL;
#endif /* COM4C0 */

static kmTimer4InpCaptureCallbackType *_timer4CallbackInpCapture = NULL;
static void *_timer4CallbackUserDataInpCapture = NULL;

static uint16_t _kmTimer4InpCaptureCycles = 0;

// "Private" functions

// "Public" functions
// Initializations
void kmTimer4Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB);
void kmTimer4InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler);
void kmTimer4InitOnAccuratePeriodGenerateOutputClockA(const uint32_t period);
void kmTimer4InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC4A);
void kmTimer4InitExternal(bool falling);

void kmTimer4InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB);
// Setters callback user data
void kmTimer4SetCallbackUserDataOVF(void *userData);
void kmTimer4SetCallbackUserDataCompA(void *userData);
void kmTimer4SetCallbackUserDataCompB(void *userData);
#ifdef COM4C0
void kmTimer4SetCallbackUserDataCompC(void *userData);
#endif /* COM4C0 */

// Registering callbacks
void kmTimer4RegisterCallbackOVF(void *userData, void (*callback)(void *));
void kmTimer4RegisterCallbackCompA(void *userData, void (*callback)(void *));
void kmTimer4RegisterCallbackCompB(void *userData, void (*callback)(void *));
#ifdef COM4C0
void kmTimer4RegisterCallbackCompC(void *userData, void (*callback)(void *));
#endif /* COM4C0 */

// Unregistering callbacks
void kmTimer4UnregisterCallbackOVF(void);
void kmTimer4UnregisterCallbackCompA(void);
void kmTimer4UnregisterCallbackCompB(void);
#ifdef COM4C0
void kmTimer4UnregisterCallbackCompC(void);
#endif /* COM4C0 */

// Enabling interrupts
void kmTimer4EnableInterruptOVF(void);
void kmTimer4EnableInterruptCompA(void);
void kmTimer4EnableInterruptCompB(void);
#ifdef COM4C0
void kmTimer4EnableInterruptCompC(void);
#endif /* COM4C0 */

// Disabling interrupts
void kmTimer4DisableInterruptsAll(void);
void kmTimer4DisableInterruptOVF(void);
void kmTimer4DisableInterruptCompA(void);
void kmTimer4DisableInterruptCompB(void);
#ifdef COM4C0
void kmTimer4DisableInterruptCompC(void);
#endif /* COM4C0 */

// Configuration of Comparator Outputs
void kmTimer4ConfigureOCA(uint8_t compareOutputMode);
void kmTimer4ConfigureOCB(uint8_t compareOutputMode);
#ifdef COM4C0
void kmTimer4ConfigureOCC(uint8_t compareOutputMode);
#endif /* COM4C0 */

// Controlling timer flow
void kmTimer4Start(void);
void kmTimer4Stop(void);
void kmTimer4Restart(void);

// Controlling PWM
void kmTimer4SetPwmDutyBottomToTop(Tcc4PwmOut pwmOut, uint16_t duty);
void kmTimer4SetPwmInversion(Tcc4PwmOut pwmOut, bool inverted);

// Timer flow calculations
uint16_t kmTimer4CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);
uint16_t kmTimer4CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy);
uint16_t kmTimer4CalcDutyOnCycles(uint16_t duty, uint16_t cycles);

// Implementation
void kmTimer4Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB) {
	kmTimer4Stop();
	_timer4PrescalerSelectBits = prescaler;
	TCCR4A = (TCCR4A & ~KM_TCC4_MODE_MASK_A) | modeA;
	TCCR4B = (TCCR4B & ~KM_TCC4_MODE_MASK_B) | modeB;
}

void kmTimer4InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler) {
	kmTimer4Init(prescaler, KM_TCC4_MODE_0_A, KM_TCC4_MODE_0_B);
}

void kmTimer4InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds) {
	kmTimer4InitOnAccurateTimeCompAInterruptCallback(periodInMicroseconds >> KMC_DIV_BY_2, true);
}

void kmTimer4InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC4A) {
	_timer4CompACycles = kmTimer4CalcPerdiod(microseconds, &_timer4PrescalerSelectBits);
	kmTimer4Init(_timer4PrescalerSelectBits, KM_TCC4_MODE_4_A, KM_TCC4_MODE_4_B);
	kmTimer4SetValueCompA(_timer4CompACycles);
	kmTimer4ConfigureOCA(outOnOC4A ? KM_TCC4_A_PWM_COMP_OUT_TOGGLE : KM_TCC4_A_PWM_COMP_OUT_NORMAL);
}

void kmTimer4InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB) {
	_timer4CompACycles = kmTimer4CalcPerdiod(microseconds, &_timer4PrescalerSelectBits);
	_timer4CompBCycles = kmTimer4CalcDutyOnCycles(phaseIntB, _timer4CompACycles);
	kmTimer4Init(_timer4PrescalerSelectBits, KM_TCC4_MODE_4_A, KM_TCC4_MODE_4_B);
	kmTimer4SetValueCompA(_timer4CompACycles);
	kmTimer4SetValueCompB(_timer4CompBCycles);
}

void kmTimer4InitOnPrescalerBottomToTopFastPwmOCA(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA) {
	kmTimer4Init(prescaler, KM_TCC4_MODE_E_A, KM_TCC4_MODE_E_B);
	kmTimer4SetValueOvf(_timer4CompOvfCycles);

	if (dutyA > KMC_UNSIGNED_ZERO) {
		kmTimer4ConfigureOCA(invertedA ? KM_TCC4_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer4SetValueCompA(dutyA);
	}
}

void kmTimer4InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA
												, const uint16_t dutyB
												, const bool invertedB
#ifdef COM4C0
												, const uint16_t dutyC
												, const bool invertedC
#endif /* COM4C0 */
												) {
	kmTimer4InitOnPrescalerBottomToTopFastPwmOCA(prescaler, dutyA, invertedA);

	if (dutyB > KMC_UNSIGNED_ZERO) {
		kmTimer4ConfigureOCB(invertedB ? KM_TCC4_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer4SetValueCompB(dutyB);
	}

#ifdef COM4C0
	if (dutyC > KMC_UNSIGNED_ZERO) {
		kmTimer4ConfigureOCC(invertedC ? KM_TCC4_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer4SetValueCompC(dutyC);
	}
#endif /* COM4C0 */
}

uint16_t kmTimer4InitOnAccurateTimeFastPwm(const uint32_t microseconds
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef COM4C0
											, const uint16_t dutyC
											, const bool invertedC
#endif /* COM4C0 */
											) {
	
	_timer4CompOvfCycles = kmTimer4CalcPerdiodWithMinPwmAccuracy(microseconds, &_timer4PrescalerSelectBits, KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer4Init(_timer4PrescalerSelectBits, KM_TCC4_MODE_E_A, KM_TCC4_MODE_E_B);
	kmTimer4SetValueOvf(_timer4CompOvfCycles);

	if (dutyA > KMC_UNSIGNED_ZERO) {
		_timer4CompACycles = kmTimer4CalcDutyOnCycles(dutyA, _timer4CompOvfCycles);
		kmTimer4SetValueCompA(_timer4CompACycles);
		kmTimer4ConfigureOCA(invertedA ? KM_TCC4_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_timer4CompBCycles = kmTimer4CalcDutyOnCycles(dutyB, _timer4CompOvfCycles);
		kmTimer4SetValueCompB(_timer4CompBCycles);
		kmTimer4ConfigureOCB(invertedB ? KM_TCC4_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#ifdef COM4C0
	if (dutyC > KMC_UNSIGNED_ZERO) {
		_timer4CompCCycles = kmTimer4CalcDutyOnCycles(dutyC, _timer4CompOvfCycles);
		kmTimer4SetValueCompC(_timer4CompCCycles);
		kmTimer4ConfigureOCC(invertedC ? KM_TCC4_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif /* COM4C0 */

	return _timer4CompOvfCycles;
}

void kmTimer4InitOnPrescalerBottomToTopPcPwmOCA(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA) {
	kmTimer4Init(prescaler, KM_TCC4_MODE_A_A, KM_TCC4_MODE_A_B);
	kmTimer4SetValueOvf(_timer4CompOvfCycles);

	kmTimer4SetValueCompA(dutyA);
	kmTimer4ConfigureOCA(invertedA ? KM_TCC4_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
}

void kmTimer4InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef COM4C0
											, const uint16_t dutyC
											, const bool invertedC
#endif /* COM4C0 */
											) {
	kmTimer4InitOnPrescalerBottomToTopPcPwmOCA(prescaler, dutyA, invertedA);

	if (dutyB > 0) {
		kmTimer4SetValueCompB(dutyB);
		kmTimer4ConfigureOCB(invertedB ? KM_TCC4_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef COM4C0
	if (dutyC > 0) {
		kmTimer4SetValueCompC(dutyC);
		kmTimer4ConfigureOCC(invertedC ? KM_TCC4_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif /* COM4C0 */
}

uint16_t kmTimer4InitOnAccurateTimePcPwm(const uint32_t microseconds
										, const uint16_t dutyA
										, const bool invertedA
										, const uint16_t dutyB
										, const bool invertedB
#ifdef COM4C0
										, const uint16_t dutyC
										, const bool invertedC
#endif /* COM4C0 */
										) {
	_timer4CompOvfCycles = kmTimer4CalcPerdiodWithMinPwmAccuracy(microseconds >> KMC_DIV_BY_2, &_timer4PrescalerSelectBits, KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer4SetValueOvf(_timer4CompOvfCycles);
	kmTimer4Init(_timer4PrescalerSelectBits, KM_TCC4_MODE_A_A, KM_TCC4_MODE_A_B);
	
	if (dutyA > KMC_UNSIGNED_ZERO) {
		_timer4CompACycles = kmTimer4CalcDutyOnCycles(dutyA, _timer4CompOvfCycles);
		kmTimer4SetValueCompA(_timer4CompACycles);
		kmTimer4ConfigureOCA(invertedB ? KM_TCC4_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_timer4CompBCycles = kmTimer4CalcDutyOnCycles(dutyB, _timer4CompOvfCycles);
		kmTimer4SetValueCompB(_timer4CompBCycles);
		kmTimer4ConfigureOCB(invertedB ? KM_TCC4_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef COM4C0
	if (dutyC > KMC_UNSIGNED_ZERO) {
		_timer4CompCCycles = kmTimer4CalcDutyOnCycles(dutyC, _timer4CompOvfCycles);
		kmTimer4SetValueCompC(_timer4CompCCycles);
		kmTimer4ConfigureOCC(invertedC ? KM_TCC4_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC4_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif /* COM4C0 */

	return _timer4CompOvfCycles;
}

void kmTimer4InitExternal(bool falling) {
	if (true == falling) {
		_timer4PrescalerSelectBits = KM_TCC4_EXT_T4_FAL;
		} else {
		_timer4PrescalerSelectBits = KM_TCC4_EXT_T4_RIS;
	}
}

void kmTimer4InitInputCapture(const uint32_t iddleTimeMicroseconds,
				void *iddleUserData, kmTimer4CallbackType *iddleCallback,
				void *inpCaptureUserData, kmTimer4InpCaptureCallbackType *inpCaptureCallback) {
	KM_TCC4_ICP4_PORT_DDR &= ~_BV(KM_TCC4_ICP4_PIN);
	KM_TCC4_ICP4_PORT_OUTPUT |= _BV(KM_TCC4_ICP4_PIN);

	kmTimer4InitOnAccurateTimeCompAInterruptCallback(iddleTimeMicroseconds, false);

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(iddleUserData), iddleCallback);
	kmTimer4RegisterCallbackInpCapture(KM_TIMER4_USER_DATA(inpCaptureUserData), inpCaptureCallback);
	kmTimer4EnableInterruptCompA();
	kmTimer4EnableInterruptInpCapture();

	kmTimer4Start();
}

uint16_t kmTimer4CalcDutyOnCycles(uint16_t duty, uint16_t cyclesRange) {
	uint32_t result = ((uint32_t)cyclesRange * (uint32_t)duty) / (uint32_t)KM_TCC4_TOP;
	return (uint16_t)result;
}

void kmTimer4SetPwmDutyAccurateTimeModes(uint16_t duty) {
	kmTimer4SetValueCompB(kmTimer4CalcDutyOnCycles(duty, _timer4CompACycles));
}

void kmTimer4SetPwmDutyBottomToTop(Tcc4PwmOut pwmOut, uint16_t duty) {
	switch (pwmOut) {
		case KM_TCC4_PWM_OUT_A: {
			kmTimer4SetValueCompA(duty);
			break;
		}
#ifdef COM4B0
		case KM_TCC4_PWM_OUT_B: {
			kmTimer4SetValueCompB(duty);
			break;
		}
#endif /* COM4B0 */
#ifdef COM4C0
		case KM_TCC4_PWM_OUT_C: {
			kmTimer4SetValueCompC(duty);
			break;
		}
#endif /* COM4C0 */
		default : {
		// intentionally
		}
	}
}

uint16_t kmTimer4CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler) {
	uint64_t cycles = (int64_t)(F_CPU);
	cycles *= microseconds;
	cycles /= KMC_CONV_MICROSECONDS_TO_SECONCS;
	if (cycles <= KM_TCC4_TOP) {
		// no prescaler, full XTAL
		*prescaler = KM_TCC4_PRSC_1;
		} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC4_TOP) {
		// prescaler by /8
		*prescaler = KM_TCC4_PRSC_8;
		} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC4_TOP) {
		// prescaler by /64
		*prescaler = KM_TCC4_PRSC_64;
		} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC4_TOP) {
		// prescaler by /256
		*prescaler = KM_TCC4_PRSC_256;
		} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC4_TOP) {
		// prescaler by /1024
		*prescaler = KM_TCC4_PRSC_1024;
		} else {
		// request was out of bounds, set as maximum
		*prescaler = KM_TCC4_PRSC_1024;
		cycles = KM_TCC4_TOP;
	}
	return (uint16_t)(cycles != KMC_UNSIGNED_ZERO ? cycles - KM_TCC4_CYCLES_CALCULATION_CORRECTION : cycles);
}

uint16_t kmTimer4CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy) {
	uint16_t result = kmTimer4CalcPerdiod(microseconds, prescaler);
	
	if (result < minimumCycleAcuracy) {
		result = minimumCycleAcuracy;
	}
	
	return result;
}

void kmTimer4SetPwmInversion(Tcc4PwmOut pwmOut, bool inverted) {
	switch (pwmOut) {
		case KM_TCC4_PWM_OUT_A: {
			if (false == inverted) {
				kmTimer4ConfigureOCA(KM_TCC4_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
				} else {
				kmTimer4ConfigureOCA(KM_TCC4_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
		case KM_TCC4_PWM_OUT_B: {
			if (false == inverted) {
				kmTimer4ConfigureOCB(KM_TCC4_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
				} else {
				kmTimer4ConfigureOCB(KM_TCC4_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#ifdef COM4C0
		case KM_TCC4_PWM_OUT_C: {
			if (false == inverted) {
				kmTimer4ConfigureOCC(KM_TCC4_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
				} else {
				kmTimer4ConfigureOCC(KM_TCC4_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#endif /* COM4C0 */
		default: {
			// intentionally
		}
	}
}

void kmTimer4Start(void) {
	TCCR4B = (TCCR4B & ~KM_TCC4_CS_MASK) | _timer4PrescalerSelectBits;
}

void kmTimer4Stop(void) {
	TCCR4B &= (TCCR4B & ~KM_TCC4_CS_MASK) | KM_TCC4_STOP;
}

void kmTimer4Restart(void) {
	TCNT4 = KMC_UNSIGNED_ZERO;
}

void kmTimer4ConfigureOCA(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC4_A_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _timer4OtputComparePinUsedA) {
		return;
	}
	
	KM_TCC4_PWM_DDR |= KM_TCC4_PWM_BV_A;		// setup port for output
	KM_TCC4_PWM_PORT |= KM_TCC4_PWM_BV_A;		// HIGH by default

	TCCR4A = (TCCR4A & ~KM_TCC4_COMP_A_MASK) | compareOutputMode;

	_timer4OtputComparePinUsedA = true;
}

void kmTimer4ConfigureOCB(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC4_B_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _timer4OtputComparePinUsedB) {
		return;
	}
	KM_TCC4_PWM_DDR |= KM_TCC4_PWM_BV_B;		// setup port for output
	KM_TCC4_PWM_PORT |= KM_TCC4_PWM_BV_B;		// HIGH by default

	TCCR4A = (TCCR4A & ~KM_TCC4_COMP_B_MASK) | compareOutputMode;

	_timer4OtputComparePinUsedB = true;
}

#ifdef COM4C0
void kmTimer4ConfigureOCC(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC4_C_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _timer4OtputComparePinUsedC) {
		return;
	}
	KM_TCC4_PWM_DDR |= KM_TCC4_PWM_BV_C;		// setup port for output
	KM_TCC4_PWM_PORT |= KM_TCC4_PWM_BV_C;		// HIGH by default

	TCCR4A = (TCCR4A & ~KM_TCC4_COMP_C_MASK) | compareOutputMode;

	_timer4OtputComparePinUsedC = true;
}
#endif /* COM4C0 */

void kmTimer4SetValueOvf(uint16_t value) {
	ICR4 = _timer4CompACycles = value; // -V2561
}

void kmTimer4SetValueCompA(uint16_t value) {
	OCR4A = _timer4CompACycles = value; // -V2561
}

#ifdef OCR4B
void kmTimer4SetValueCompB(uint16_t value) {
	OCR4B = _timer4CompBCycles = value; // -V2561
}
#endif /* OCR4B */


#ifdef COM4C0
void kmTimer4SetValueCompC(uint16_t value) {
	OCR4C = _timer4CompCCycles = value; // -V2561
}
#endif /* COM4C0 */

uint16_t kmTimer4GetValueOvf(void) {
	return _timer4CompOvfCycles;
}

uint16_t kmTimer4GetValueCompA(void) {
	return _timer4CompACycles;
}

uint16_t kmTimer4GetValueCompB(void) {
	return _timer4CompBCycles;
}

#ifdef COM4C0
uint16_t kmTimer4GetValueCompC(void) {
	return _timer4CompCCycles;
}
#endif /* COM4C0 */

void kmTimer4SetPrescale(uint8_t prescaler) {
	_timer4PrescalerSelectBits = prescaler;
	kmTimer4Start();
}

void kmTimer4EnableInterruptCompA(void) {
	TIMSK4 |= _BV(OCIE4A);
}

void kmTimer4EnableInterruptCompB(void) {
	TIMSK4 |= _BV(OCIE4B);
}

#ifdef COM4C0
void kmTimer4EnableInterruptCompC(void) {
	TIMSK4 |= _BV(OCIE4C);
}
#endif /* COM4C0 */

void kmTimer4EnableInterruptOVF(void) {
	TIMSK4 |= _BV(TOIE4);
}

void kmTimer4EnableInterruptInpCapture(void) {
	TIMSK4 |= _BV(ICIE4);
}

void kmTimer4DisableInterruptCompA(void) {
	TIMSK4 &= ~_BV(OCIE4A);
}

void kmTimer4DisableInterruptCompB(void) {
	TIMSK4 &= ~_BV(OCIE4B);
}

#ifdef COM4C0
void kmTimer4DisableInterruptCompC(void) {
	TIMSK4 &= ~_BV(OCIE4C);
}
#endif /* COM4C0 */

void kmTimer4DisableInterruptOVF(void) {
	TIMSK4 &= ~_BV(TOIE4);
}

void kmTimer4DisableInterruptInpCapture(void) {
	TIMSK4 &= ~_BV(ICIE4);
}

void kmTimer4DisableInterruptsAll(void) {
#ifdef COM4C0
	TIMSK4 &= ~(_BV(OCIE4C) | _BV(OCIE4B) | _BV(OCIE4A) | _BV(TOIE4 | _BV(ICIE4)));
#else /* COM4C0 */
	TIMSK4 &= ~(_BV(OCIE4B) | _BV(OCIE4A) | _BV(TOIE4 | _BV(ICIE4)));
#endif /* COM4C0 */
}

void kmTimer4RegisterCallbackCompA(void *userData, kmTimer4CallbackType *callback) {
	_timer4CallbackCompA = callback;
	_Timer4CallbackUserDataCompA = userData;
}

void kmTimer4RegisterCallbackCompB(void *userData, kmTimer4CallbackType *callback) {
	_timer4CallbackCompB = callback;
	_timer4CallbackUserDataCompB = userData;
}

#ifdef COM4C0
void kmTimer4RegisterCallbackCompC(void *userData, kmTimer4CallbackType *callback) {
	_timer4CallbackCompC = callback;
	_timer4CallbackUserDataCompC = userData;
}
#endif /* COM4C0 */

void kmTimer4RegisterCallbackOVF(void *userData, kmTimer4CallbackType *callback) {
	_timer4CallbackOVF = callback;
	_timer4CallbackUserDataOVF = userData;
}

void kmTimer4RegisterCallbackInpCapture(void *userData, kmTimer4InpCaptureCallbackType *callback) {
	_timer4CallbackInpCapture = callback;
	_timer4CallbackUserDataInpCapture = userData;
}

void kmTimer4UnregisterCallbackCompA(void) {
	_Timer4CallbackUserDataCompA = NULL;
}

void kmTimer4UnregisterCallbackCompB(void) {
	_timer4CallbackUserDataCompB = NULL;
}

#ifdef COM4C0
void kmTimer4UnregisterCallbackCompC(void) {
	_timer4CallbackUserDataCompC = NULL;
}
#endif /* COM4C0 */

void kmTimer4UnregisterCallbackOVF(void) {
	_timer4CallbackUserDataOVF = NULL;
}

void kmTimer4UnregisterCallbackInpCapture(void) {
	_timer4CallbackInpCapture = NULL;
}

void kmTimer4SetCallbackUserDataOVF(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_timer4CallbackUserDataOVF = userData;
	}
}

void kmTimer4SetCallbackUserDataInpCapture(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_timer4CallbackUserDataInpCapture = userData;
	}
}

void kmTimer4SetCallbackUserDataCompA(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer4CallbackUserDataCompA = userData;
	}
}

void kmTimer4SetCallbackUserDataCompB(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_timer4CallbackUserDataCompB = userData;
	}
}

#ifdef COM4C0
void kmTimer4SetCallbackUserDataCompC(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_timer4CallbackUserDataCompC = userData;
	}
}
#endif /* COM4C0 */

bool kmTimer4InpCaputureGetState(void) {
	return (KM_TCC4_ICP4_PORT_INPUT & _BV(KM_TCC4_ICP4_PIN));
}

ISR(TIMER4_COMPA_vect, ISR_NOBLOCK) {
	if (NULL != _timer4CallbackCompA) {
		_timer4CallbackCompA(_Timer4CallbackUserDataCompA);
	}
}

ISR(TIMER4_COMPB_vect, ISR_NOBLOCK) {
	if (NULL != _timer4CallbackCompB) {
		_timer4CallbackCompB(_timer4CallbackUserDataCompB);
	}
}

#ifdef COM4C0
ISR(TIMER4_COMPC_vect, ISR_NOBLOCK) {
	if (NULL != _timer4CallbackCompC) {
		_timer4CallbackCompC(_timer4CallbackUserDataCompC);
	}
}
#endif /* COM4C0 */

ISR(TIMER4_OVF_vect, ISR_NOBLOCK) {
	if (NULL != _timer4CallbackOVF) {
		_timer4CallbackOVF(_timer4CallbackUserDataOVF);
	}
}

ISR(TIMER4_CAPT_vect, ISR_NOBLOCK) {
	_kmTimer4InpCaptureCycles = ICR4;
	TCCR4B ^= _BV(ICES4);
	kmTimer4Restart();
	if (NULL != _timer4CallbackInpCapture) {
		_timer4CallbackInpCapture(_kmTimer4InpCaptureCycles, kmTimer4InpCaputureGetState(), _timer4CallbackUserDataInpCapture);
	}
}
#endif /* TIMSK4 */
