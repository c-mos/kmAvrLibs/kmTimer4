/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @mainpage
* @section pageTOC Content
* @brief The kmTimer4 library provides functions to initialize and control Timer 0 on AVR MCUs.
* - kmTimer4.h
* - kmTimer4DefaultConfig.h
*
*  **Created on**: 3/15/2024 11:21:05 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2024 Krzysztof Moskwa
*  kmTimer4.h
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef KMTIMER4_H_
#define KMTIMER4_H_

#ifdef KM_DOXYGEN
#define TIMSK4
#define COM4C0
#endif /* KM_DOXYGEN */


#ifdef __cplusplus
extern "C" {
#endif
#ifdef TIMSK4

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>

#include "../kmCommon/kmCommon.h"
#include "../kmTimersCommon/kmTimer4Defs.h"

/// Remapping macro for saving own user data in callback registration #timer4RegisterCallbackCompA function
#define KM_TIMER4_USER_DATA(X) (void *)(X)

/**
Definition of the Timer4 Callback Type
@param Pointer for void user data content that is used in #timer4RegisterCallbackCompA function
and will be delivered to callback."as is"
*/
typedef void kmTimer4CallbackType(void *);

/**
@brief Callback type for Timer 4 input capture events.

This is the function pointer type for the callback function that will be
called when a Timer 4 input capture event occurs.

@param[out] captureValue The captured value from the Timer 4 input capture.
@param[out] captureState The input state information obtained during the capture event.
@param[out] userData A pointer to user-defined data that was registered with the callback.

@note The callback function must adhere to this signature to be compatible
      with the Timer 4 input capture system.
*/
typedef void kmTimer4InpCaptureCallbackType(const uint16_t, const bool, const void *);

/**
Enumeration type definition of the PWM outputs for timer used in parameters of #kmTimer4SetPwmDuty, #kmTimer4SetPwmDuty, #kmTimer4SetPwmInversion functions .
*/
typedef enum {
	/// PWM on output A.
	KM_TCC4_PWM_OUT_A,
	/// PWM on output B.
	KM_TCC4_PWM_OUT_B,
#ifdef COM4C0
	/// PWM on output C.
	KM_TCC4_PWM_OUT_C
#endif /* COM4C0 */
} Tcc4PwmOut;

/**
@brief Initializes Timer 4 in a mode allowing the use of CompA, CompB, CompC, and Overflow Interrupts launched at a specific phase of the timer.

The timer always counts from 0 to KM_\b Timer4_MAX value (255). Callbacks for CompA, CompB, and CompC are launched
once the timer reaches specific values set by #Timer4SetValueCompA, #Timer4SetValueCompB, and #Timer4SetValueCompC.
The callbacks need to be configured with #kmTimer4RegisterCallbackOVF, #kmTimer4RegisterCallbackCompA,
#kmTimer4RegisterCallbackCompB, and #kmTimer4RegisterCallbackCompC functions. The period/frequency of each of these Interrupts
will be exactly the same and defined by MCU main clock and the prescaler.
The Overflow callback is called when Timer 4 reaches the value \b KM_TIMER4_MAX (255).
CompA, CompB, and CompC callbacks are called when Timer 4 reaches values set by #Timer4SetValueCompA, #Timer4SetValueCompB, and #Timer4SetValueCompC respectively.
Setting these values allows shifting callback calls with phase (with accuracy limited by the time that the MCU takes to execute code within the callback).

@param prescaler Selects one of the prescalers as defined in kmTimersCommon/kmTimer4Defs.h (e.g., \b KM_TCC4_PRSC_256).

@code
#define KM_TIMER4_TEST_USER_DATA_A 1UL
#define KM_TIMER4_TEST_USER_DATA_B 65535UL
#define KM_TIMER4_TEST_USER_DATA_C 255UL
#define KM_TIMER4_TEST_USER_DATA_D 127UL
#define KM_TIMER4_TEST_DUTY_0_PERC KM_TIMER4_BOTTOM
#define KM_TIMER4_TEST_DUTY_25_PERC KM_TIMER4_MID - (KM_TIMER4_MID >> 1)
#define KM_TIMER4_TEST_DUTY_50_PERC KM_TIMER4_MID
#define KM_TIMER4_TEST_DUTY_75_PERC KM_TIMER4_MID + (KM_TIMER4_MID >> 1)
#define KM_TIMER4_TEST_DUTY_100_PERC KM_TIMER4_TOP

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"
#include "kmTimersCommon/kmTimerDefs.h"
#include "kmTimer4/kmTimer4.h"

void callbackOVF(void *userData) {
    dbToggle(DB_PIN_0);
    dbOn(DB_PIN_1);
    dbOn(DB_PIN_2);
    dbOn(DB_PIN_3);
}

void callbackCompAOff(void *userData) {
    dbOff(DB_PIN_1);
}

void callbackCompBOff(void *userData) {
    dbOff(DB_PIN_2);
}

void callbackCompCOff(void *userData) {
    dbOff(DB_PIN_3);
}

int main(void) {
    appInitDebug();
    kmCpuInterruptsEnable();

    kmTimer4InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(KM_TCC4_PRSC_8);

    kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackOVF);
    kmTimer4EnableInterruptOVF();

    kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAOff);
    kmTimer4SetValueCompA(KM_TIMER4_TEST_DUTY_25_PERC);
    kmTimer4EnableInterruptCompA();

    kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompBOff);
    kmTimer4SetValueCompB(KM_TIMER4_TEST_DUTY_75_PERC);
    kmTimer4EnableInterruptCompB();

    kmTimer4RegisterCallbackCompC(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_D), callbackCompCOff);
    kmTimer4SetValueCompC(KM_TIMER4_TEST_DUTY_50_PERC);
    kmTimer4EnableInterruptCompC();

    kmTimer4Start();

    while(true) {
    }
@endcode

Output from the example code:
\image html kmTimer4Test2.png
 */
void kmTimer4InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler);

/**
Initializes Timer 4 in the mode allowing to generate frequency with specific period on OCA0 output.
This function tries to configure Timer4 to make the period as accurate as possible -
see limitations in the description of #kmTimer4CalcPerdiod function.
To change period while Timer4 is running calculate new CompA value and use #kmTimer4SetValueCompA and #kmTime0SetPrescale functions.
with kmTimer4CalcPerdiod shifting new periodInMicroseconds by one bit to the right
@param periodInMicroseconds period of the square function on the OCA0, the available range depends on the main MCU clock

Example code:
@code
	kmTimer4InitOnAccuratePeriodGenerateOutputClockA(KM_TIMER4_TEST_1MS);
	kmTimer4Start();
@endcode
Output from the example code:
\image html kmTimer4Test0.png
*/
void kmTimer4InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds);

/**
Initializes Timer 4 in the mode allowing to use CompA Interrupt launched every specific period defined in microseconds.
The callback needs to be configured with #kmTimer4RegisterCallbackCompA function.
It's also possible to generate the square wave toggling output with the same period (output frequency is half of frequency of executing callbacks)
Note accuracy is limited with time that used by MCU to execute code within callback.
@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock

@code
	
(KM_TIMER4_TEST_5MS, true);

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer4EnableInterruptCompA();

	kmTimer4Start();
}
@endcode
Output from the example code:
\image html kmTimer4Test1.png
*/
void kmTimer4InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC4A);

/**
Initializes Timer 4 as a Counter of either rising or falling signal edges on the input T0
@param falling if true the counter will count falling edges, if false - rising edges
*/
void kmTimer4InitExternal(bool falling);

/**
Initializes Timer 4 in the mode allowing to use CompA and CompB Interrupt launched every specific period defined in microseconds.
The CompB Interrupt can be shifted in phase by providing corresponding phaseIntB parameter
The callbacks needs to be configured with #kmTimer4RegisterCallbackCompA and #kmTimer4RegisterCallbackCompB functions.
The timer goes always from 0 to top value defined as calculation for microseconds used as parameter.
This function tries to define period of calling the callbacks as accurate as possible
The range of available frequencies depends on used XTAL\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock
@param phaseIntB phase shift between executing CompA and CompB callbacks. The range of the value from 0 to 255, where 0 means no phase shift and 128 means 180 phase shift.
The accuracy of the phase shift depends on on the main MCU clock and the microseconds parameter

@code
	kmTimer4InitOnAccurateTimeCompABInterruptCallback(KM_TIMER4_TEST_1MS, KM_TIMER4_TEST_PHASE_180_DEG); // for 1 millisecond

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer4EnableInterruptCompA();

	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer4EnableInterruptCompB();
	kmTimer4Start();
@endcode
Output from the example code:
\image html kmTimer4Test5.png
*/
void kmTimer4InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB);

/**
Initializes Timer 4 to generate square wave on the OC4x pin of the MCU with specified time period and duty cycle (Fast PWM mode)
To change the duty cycle after initialization #kmTimer4SetPwmDuty function can be used
(only #KM_TCC4_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC4_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 4 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC4x pin of MCU, the available range and accuracy depends on the main MCU clock
@param dutyA duty of the square wave generated on the OC4A pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer4DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC4A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC4A pin is inverted
@param dutyB duty of the square wave generated on the OC4B pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer4DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC4B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC4B pin is inverted
@param dutyC Duty of the square wave generated on the OC4C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer4DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC4C pin is inverted.

@code
	uint16_t cyclesRange = kmTimer4InitOnAccurateTimeFastPwm(KM_Timer4_TEST_1MS, true, KM_Timer4_TEST_DUTY_25_PERC, false);

	kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackOVFToggle);
	kmTimer4EnableInterruptOVF();

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer4EnableInterruptCompA();

	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompBToggle);
	kmTimer4EnableInterruptCompB();

	kmTimer4Start();

	// to change duty on the channel B
	testSwipePwm(KM_TCC4_PWM_OUT_B, cyclesRange);

#endif	
@endcode
Output from the example code:
\image html kmTimer4Test5.png
*/

#ifdef COM4C0
uint16_t kmTimer4InitOnAccurateTimeFastPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM4C0 */
uint16_t kmTimer4InitOnAccurateTimeFastPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM4C0 */

/**
Initializes Timer 4 to generate square wave on the OC4x pin of the MCU with specified time period and duty cycle (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer4SetPwmDuty function can be used
(only #KM_TCC4_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC4_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 4 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC4x pin of MCU, the available range and accuracy depends on the main MCU clock
@param dutyA duty of the square wave generated on the OC4A pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer4DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC4A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC4A pin is inverted
@param dutyB duty of the square wave generated on the OC4B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer4DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC4B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC4B pin is inverted
@param dutyC Duty of the square wave generated on the OC4C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer4DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC4C pin is inverted.

@code
	uint16_t cyclesRange = kmTimer4InitOnAccurateTimePcPwm(KM_TIMER4_TEST_1MS, true, KM_Timer4_TEST_DUTY_25_PERC, false);
	kmTimer4Start();

	kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackOVFToggle);
	kmTimer4EnableInterruptOVF();

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer4EnableInterruptCompA();

	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompBToggle);
	kmTimer4EnableInterruptCompB();

	testSwipePwm(KM_TCC4_PWM_OUT_B, KM_TIMER4_MAX);
@endcode
Output from the example code:
\image html kmTimer4Test7.png
*/
#ifdef COM4C0
uint16_t kmTimer4InitOnAccurateTimePcPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM4C0 */
uint16_t kmTimer4InitOnAccurateTimePcPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM4C0 */

/**
IInitializes Timer 4 to generate square wave on the OC4x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Fast PWM mode)
To change the duty cycle after initialization #kmTimer4SetPwmDuty function can be used (for both #KM_TCC4_PWM_OUT_A and #KM_TCC4_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 4 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer4Defs.h (e.g. KM_TCC4_PRSC_256)
@param dutyA duty of the square wave generated on the OC4A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC4A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC4A pin is inverted
@param dutyB duty of the square wave generated on the OC4B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC4B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC4B pin is inverted
@param dutyC Duty of the square wave generated on the OC4C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer4DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC4C pin is inverted.

@code
kmTimer4InitOnPrescalerBottomToTopFastPwm(KM_TCC4_PRSC_64, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_75_PERC, false);

kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackOVFToggle);
kmTimer4EnableInterruptOVF();

kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAToggle);
kmTimer4EnableInterruptCompA();

kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBToggle);
kmTimer4EnableInterruptCompB();

kmTimer4Start();

testSwipePwm(KM_TCC4_PWM_OUT_A, KM_TIMER4_MAX);
testSwipePwm(KM_TCC4_PWM_OUT_B, KM_TIMER4_MAX);
@endcode
Output from the example code:
\image html kmTimer4Test4.png
*/
#ifdef COM4C0
void kmTimer4InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM4C0 */
void kmTimer4InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM4C0 */

/**
Initializes Timer 4 to generate square wave on the OC4x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer4SetPwmDuty function can be used (for both #KM_TCC4_PWM_OUT_A and #KM_TCC4_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 4 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer4Defs.h (e.g. KM_TCC4_PRSC_256)
@param dutyA duty of the square wave generated on the OC4A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC4A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC4A pin is inverted
@param dutyB duty of the square wave generated on the OC4B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC4B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC4B pin is inverted
@param dutyC Duty of the square wave generated on the OC4C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer4DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC4C pin is inverted.

@code
kmTimer4InitOnPrescalerBottomToTopPcPwm(KM_TCC4_PRSC_64, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_75_PERC, false);

kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackOVFToggle);
kmTimer4EnableInterruptOVF();

kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAToggle);
kmTimer4EnableInterruptCompA();

kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBToggle);
kmTimer4EnableInterruptCompB();

kmTimer4Start();

testSwipePwm(KM_TCC4_PWM_OUT_A, KM_TIMER4_MAX);
testSwipePwm(KM_TCC4_PWM_OUT_B, KM_TIMER4_MAX);
@endcode
Output from the example code:
\image html kmTimer4Test6.png
*/
#ifdef COM4C0
void kmTimer4InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM4C0 */
void kmTimer4InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM4C0 */

/**
@brief Initializes Timer/Counter 4 for input capture with specified parameters.

This function initializes Timer/Counter 4 for input capture mode, configuring it
with the specified parameters. It sets up the necessary callbacks, enables interrupts,
and starts the timer for accurate timing measurements.
The iddle time defines maximum time of captured input state change.

@param iddleTimeMicroseconds The idle time (in microseconds) before capturing signals.
@param iddleUserData User data to be passed to the idle time callback function.
@param iddleCallback Pointer to the callback function for the idle time interrupt.
@param inpCaptureUserData User data to be passed to the input capture callback function.
@param inpCaptureCallback Pointer to the callback function for the input capture interrupt.
 */
void kmTimer4InitInputCapture(const uint32_t iddleTimeMicroseconds,
							void *iddleUserData, kmTimer4CallbackType *iddleCallback,
							void *inpCaptureUserData, kmTimer4InpCaptureCallbackType *inpCaptureCallback);

// Setters callback user data
/**
Sets or changes User Data passed to the callback registered with #kmTimer4RegisterCallbackOVF.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER4_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer4SetCallbackUserDataOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C));
@endcode
*/
void kmTimer4SetCallbackUserDataOVF(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer4RegisterCallbackCompA.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER4_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A));
@endcode
*/
void kmTimer4SetCallbackUserDataCompA(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer4RegisterCallbackCompB.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER4_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B));
@endcode
*/
void kmTimer4SetCallbackUserDataCompB(void *userData);

#ifdef COM4C0
/**
Sets or changes User Data passed to the callback registered with #kmTimer4RegisterCallbackCompC.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER4_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer4RegisterCallbackCompC(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B));
@endcode
*/
void kmTimer4SetCallbackUserDataCompC(void *userData);
#endif /* COM4C0 */

// Registering callbacks
/**
Functions to register callback launched on Timer4 Overflow. The callback needs to be declared with #kmTimer4CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER4_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer4CallbackType type

@code
void callbackOVFToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_0);
}

/// application routine
kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackOVFToggle);
@endcode
*/
void kmTimer4RegisterCallbackOVF(void *userData, kmTimer4CallbackType *callback);

/**
Functions to register callback launched on Timer4 Overflow Interrupt. The callback needs to be declared with #kmTimer4CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER4_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer4CallbackType type

@code
void callbackCompAToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_0);
}

/// application routine
kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAToggle);
@endcode
*/
void kmTimer4RegisterCallbackCompA(void *userData, kmTimer4CallbackType *callback);

/**
Functions to register callback launched on Timer4 Overflow Interrupt. The callback needs to be declared with #kmTimer4CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER4_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer4CallbackType type

@code
void callbackCompBToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_1);
}

/// application routine
kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBToggle);
@endcode
*/
void kmTimer4RegisterCallbackCompB(void *userData, kmTimer4CallbackType *callback);

#ifdef COM4C0
/**
Functions to register callback launched on Timer4 Overflow Interrupt. The callback needs to be declared with #kmTimer4CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER4_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer4CallbackType type

@code
void callbackCompCToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_2);
}

/// application routine
kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompCToggle);
@endcode
*/
void kmTimer4RegisterCallbackCompC(void *userData, kmTimer4CallbackType *callback);
#endif /* COM4C0 */

/**
@brief Register a callback function for Timer 4 input capture event.

This function registers a user-provided callback function that will be
called when a Timer 4 input capture event occurs. The user can also
provide a custom data pointer which will be passed back to the callback
when it is invoked.

@param[in] userData A pointer to user-defined data that will be passed to
                    the callback function.
@param[in] callback A pointer to the function that will be called on Timer 4
                    input capture event. This function must match the
                    signature defined by kmTimer5InpCaptureCallbackType.

@note The callback function must be set up before enabling the Timer 4 input
      capture to ensure that events are handled correctly.
*/
void kmTimer4RegisterCallbackInpCapture(void *userData, kmTimer4InpCaptureCallbackType *callback);

// Unregistering callbacks
/**
Permanently unregisters callback registered with #kmTimer4RegisterCallbackOVF function
*/
void kmTimer4UnregisterCallbackOVF(void);

/**
Permanently unregisters callback registered with #kmTimer4RegisterCallbackCompA function
*/
void kmTimer4UnregisterCallbackCompA(void);

/**
Permanently unregisters callback registered with #kmTimer4RegisterCallbackCompB function.
*/
void kmTimer4UnregisterCallbackCompB(void);

#ifdef COM4C0
/**
Permanently unregisters callback registered with #kmTimer4RegisterCallbackCompC function.
*/
void kmTimer4UnregisterCallbackCompC(void);
#endif /* COM4C0 */

/**
Permanently unregisters callback registered with #kmTimer4RegisterCallbackInpCapture function.
*/
void kmTimer4UnregisterCallbackInpCapture(void);

// Enabling interrupts
/**
Enables Timer4 Overflow interrupt allowing to execute routine registered with #kmTimer4RegisterCallbackOVF
*/
void kmTimer4EnableInterruptOVF(void);

/**
Enables Timer4 Comparator A interrupt allowing to execute routine registered with #kmTimer4RegisterCallbackCompA
*/
void kmTimer4EnableInterruptCompA(void);

/**
Enables Timer4 Comparator B interrupt allowing to execute routine registered with #kmTimer4RegisterCallbackCompB
*/
void kmTimer4EnableInterruptCompB(void);

#ifdef COM4C0
/**
Enables Timer4 Comparator C interrupt allowing to execute routine registered with #kmTimer4RegisterCallbackCompC
*/
void kmTimer4EnableInterruptCompC(void);
#endif /* COM4C0 */

/**
@brief Enables Timer/Counter 4 Input Capture Interrupt.

This function sets the Input Capture Interrupt Enable bit for Timer/Counter 4.
After calling this function, the timer will generate an interrupt when an input capture event occurs.
The specific input capture event conditions are determined by the timer configuration and mode.
Ensure that the corresponding input capture callback is registered using #kmTimer4RegisterCallbackInpCapture
before enabling the input capture interrupt.

Example usage:
@code
kmTimer4EnableInterruptInpCapture();
@endcode
 */
void kmTimer4EnableInterruptInpCapture(void);

// Disabling interrupts
/**
Disables all Timer 4 interrupts (Overflow, ComparatorA, ComparatorB, ComparatorC & Input Capture).
*/
void kmTimer4DisableInterruptsAll(void);

/**
Disables Timer 4 Overflow interrupt
*/
void kmTimer4DisableInterruptOVF(void);

/**
Disables Timer 4 ComparatorA interrupt
*/
void kmTimer4DisableInterruptCompA(void);

/**
Disables Timer 4 ComparatorB interrupt
*/
void kmTimer4DisableInterruptCompB(void);

#ifdef COM4C0
/**
Disables Timer 4 ComparatorC interrupt
*/
void kmTimer4DisableInterruptCompC(void);
#endif /* COM4C0 */

/**
@brief Sets the value for Timer/Counter 4 Overflow.

This function sets the value for Timer/Counter 4 Overflow. The timer counts from 0 to this value
before generating an overflow interrupt and resetting to zero. Ensure that the corresponding
overflow callback is registered using #kmTimer4RegisterCallbackOVF before setting the overflow value.

@param value The value to set for Timer/Counter 4 Overflow.

Example usage:
@code
kmTimer4SetValueOvf(65535); // Set overflow value to the maximum (16-bit)
@endcode
 */
void kmTimer4SetValueOvf(uint16_t value);

// Configuration of Comparator Outputs
/**
Configures Compare Output Mode channel A for Timer4 (\b OC4A output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer4Defs.h (e.g. \b KM_TCC4_A_COMP_OUT_TOGGLE)
*/
void kmTimer4ConfigureOCA(uint8_t compareOutputMode);

/**
Configures Compare Output Mode channel B for Timer4 (\b OC4B output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer4Defs.h (e.g. \b KM_TCC4_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN)
*/
void kmTimer4ConfigureOCB(uint8_t compareOutputMode);

/**
Configures Compare Output Mode channel C for Timer4 (\b OC4C output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in
kmTimersCommon/kmTimer4Defs.h (e.g. \b KM_TCC4_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN)
*/
void kmTimer4ConfigureOCC(uint8_t compareOutputMode);

// Controlling timer flow
/**
Enables running of Timer 4, allowing to execute Timer/Counter function defined with Initialization functions
*/
void kmTimer4Start(void);

/**
Stops running of Timer 4
*/
void kmTimer4Stop(void);

/**
Restarts internal counter of Timer 4.
*/
void kmTimer4Restart(void);

// Controlling PWM
/**
Sets the Duty in the PWM modes initialized as Bottom-To-Top. Can be applied to both OC4A and OC4B outputs
@param pwmOut either KM_TCC4_PWM_OUT_A for \b OC4A or KM_TCC4_PWM_OUT_B for \b OC4B
@param duty duty of the square wave generated on the OC4A/OC4B pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle
*/

void kmTimer4SetPwmDutyBottomToTop(Tcc4PwmOut pwmOut, uint16_t duty);
/**
Sets the Duty in the PWM modes initialized as Accurate-Time. Can be applied to OC4B output only (as Comparator A is used to control general time period)
@param duty duty of the square wave generated on the OC4A/OC4B pin of the MCU; the range from 0 to 255,
where 64 means 25%/75% and 128 means 50%/50% duty cycle; this initialization offers maximum accuracy of the duty cycle
The accuracy of the value depends on the main clock of the MCU and \e microseconds parameter provided in initialization function
The function calculates it in the way to be at least equal or higher than #KM_TCC4_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer4DefaultConfig
*/
void kmTimer4SetPwmDutyAccurateTimeModes(uint16_t duty);

/**
Function allows to invert PWM output either on \b OC4A or on \b OC4B
@param pwmOut either KM_TCC4_PWM_OUT_A for \b OC4A or KM_TCC4_PWM_OUT_B for \b B
@param inverted value true inverts the PWM output, normal operation when falseOC3
*/
void kmTimer4SetPwmInversion(Tcc4PwmOut pwmOut, bool inverted);

// Timer flow calculations
/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period\n

@param microseconds time period of the expected Comparator matches for Timer 4
the available range and accuracy depends on the main MCU clock as specified in the table below
\b NOTE: Some modes of Timer4 require to divide number of microseconds by 2, this happens for example for
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.\n
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer4Defs.h to be used in #kmTimer4Init
@return comparator match value to be used in Timer initialization #kmTimer4Init function
*/
uint16_t kmTimer4CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);

/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period. This version takes into consideration minim resolution of PWM duty and decreases frequency if needed\n
@param microseconds time period of the expected Comparator matches for Timer 4
the available range and accuracy depends on the main MCU clock as specified in the table below.
Note this version of the function takes into consideration desired minimum resolution of the output square wave duty.
In practice this may only matter for very low main clock frequencies. If the desired minimum resolution cannot be reached,
this function will return prescaler and comparator match value decreasing desired period defined in milliseconds
\b NOTE: Some modes of Timer4 require to divide number of microseconds by 2, this happens for example for
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer4Defs.h to be used in #kmTimer4Init
@param minimumCycleAcuracy desired minimum resolution of the output square wave duty (e.g. 4 means that at least 0%, 25%, 50% and 75% are achievable)
@return comparator match value to be used in Timer initialization #kmTimer4Init function
*/
uint16_t kmTimer4CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy);

/**
Calculates compare match result based on desired square wave duty and available compare match cycles
calculated for Accurate-Time modes (e.g. got as result of #kmTimer4CalcPerdiod or #kmTimer4InitOnAccurateTimeFastPwm)
@param duty desired duty cycle
@param cyclesRange available range of cycles for duty cycle
@return comparator match value corresponding to desired duty cycle (can be used e.g. in #kmTimer4SetValueCompA)
*/
uint16_t kmTimer4CalcDutyOnCycles(uint16_t duty, uint16_t cyclesRange);


/**
@brief Retrieves the current value of Timer/Counter 4 Overflow.

This function returns the current value of Timer/Counter 4 Overflow, representing the number of cycles
before the timer overflows and triggers an overflow interrupt.

@return The current value of Timer/Counter 4 Overflow.

Example usage:
@code
uint16_t ovfValue = kmTimer4GetValueOvf();
@endcode
 */
uint16_t kmTimer4GetValueOvf(void);

/**
@brief Retrieves the current value of Timer/Counter 4 Compare A.

This function returns the current value of Timer/Counter 4 Compare A, representing the number of cycles
before the timer triggers a Compare A interrupt.

@return The current value of Timer/Counter 4 Compare A.

Example usage:
@code
uint16_t compAValue = kmTimer4GetValueCompA();
@endcode
 */
uint16_t kmTimer4GetValueCompA(void);

/**
@brief Retrieves the current value of Timer/Counter 4 Compare B.

This function returns the current value of Timer/Counter 4 Compare B, representing the number of cycles
before the timer triggers a Compare B interrupt.

@return The current value of Timer/Counter 4 Compare B.

Example usage:
@code
uint16_t compBValue = kmTimer4GetValueCompB();
@endcode
 */
uint16_t kmTimer4GetValueCompB(void);

#ifdef COM4C0
/**
@brief Retrieves the current value of Timer/Counter 4 Compare C.

This function returns the current value of Timer/Counter 4 Compare C, representing the number of cycles
before the timer triggers a Compare C interrupt.

@return The current value of Timer/Counter 4 Compare C.

Example usage:
@code
uint16_t compCValue = kmTimer4GetValueCompC();
@endcode
 */
uint16_t kmTimer4GetValueCompC(void);
#endif /* COM4C0 */

/**
@brief Sets the value for Timer/Counter 4 Overflow.

This function sets the value for Timer/Counter 4 Overflow. The timer counts from 0 to this value
before generating an overflow interrupt and resetting to zero. The value is also stored internally
for future reference.

@param value The value to set for Timer/Counter 4 Overflow.

Example usage:
@code
kmTimer4SetValueOvf(65535); // Set overflow value to the maximum (16-bit)
@endcode
 */
void kmTimer4SetValueOvf(uint16_t value);

/**
@brief Sets the value for Timer/Counter 4 Compare A.

This function sets the value for Timer/Counter 4 Compare A. The timer counts from 0 to this value
before triggering a Compare A interrupt. The value is also stored internally for future reference.

@param value The value to set for Timer/Counter 4 Compare A.

Example usage:
@code
kmTimer4SetValueCompA(5000); // Set Compare A value to 5000
@endcode
 */
void kmTimer4SetValueCompA(uint16_t value);

/**
@brief Sets the value for Timer/Counter 4 Compare B.

This function sets the value for Timer/Counter 4 Compare B. The timer counts from 0 to this value
before triggering a Compare B interrupt. The value is also stored internally for future reference.

@param value The value to set for Timer/Counter 4 Compare B.

Example usage:
@code
kmTimer4SetValueCompB(3000); // Set Compare B value to 3000
@endcode
 */
void kmTimer4SetValueCompB(uint16_t value);

#ifdef COM4C0
/**
@brief Sets the value for Timer/Counter 4 Compare C.

This function sets the value for Timer/Counter 4 Compare C. The timer counts from 0 to this value
before triggering a Compare C interrupt. The value is also stored internally for future reference.

@param value The value to set for Timer/Counter 4 Compare C.

Example usage:
@code
kmTimer4SetValueCompC(1000); // Set Compare C value to 1000
@endcode
 */
void kmTimer4SetValueCompC(uint16_t value);
#endif /* COM4C0 */

/**
@brief Gets the state of Timer/Counter 4 Input Capture pin.

This function returns the current state of the Timer/Counter 4 Input Capture pin.
It checks whether the Input Capture pin is currently high or low.

@return The state of the Timer/Counter 4 Input Capture pin:
        - \c true if the Input Capture pin is high.
        - \c false if the Input Capture pin is low.

Example usage:
@code
bool inpCaptureState = kmTimer4InpCaputureGetState();
if (inpCaptureState) {
    // Input Capture pin is high
} else {
    // Input Capture pin is low
}
@endcode
 */
bool kmTimer4InpCaputureGetState(void);

/**
@brief Initializes Timer 4 with the specified prescaler and modes.

This function initializes Timer 4 with the specified prescaler and modes.

@param prescaler The prescaler value to be set for Timer 4.
@param modeA The mode value to be set for Timer 4 control register A (TCCR4A).
@param modeB The mode value to be set for Timer 4 control register B (TCCR4B).

@note This function stops Timer 4 before initializing it.

@see kmTimer4Stop()
 */
void kmTimer4Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB);

/**
@brief Starts Timer 4 with the previously configured prescaler.

This function starts Timer 4 with the previously configured prescaler.
 */
void kmTimer4Start(void);

/**
@brief Stops Timer 4.

This function stops Timer 4.
 */
void kmTimer4Stop(void);

/**
@brief Restarts Timer 4 by resetting its counter value to zero.

This function restarts Timer 4 by resetting its counter value to zero.
 */
void kmTimer4Restart(void);

#endif /* TIMSK4 */
#ifdef __cplusplus
}
#endif /*  __cplusplus */
#endif /* KMTIMER4_H_ */

